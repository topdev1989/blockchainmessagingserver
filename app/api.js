var express = require('express');
var userService = require('app/services/user.service');
var twilioService = require('app/services/twilio.service');

var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'API 1.0' });
});
router.get('/version', function(req, res, next) {
  res.status(200).send({'version': '1.0'})
});

router.post('/user/auth', verifyPhone);
router.post('/user/verfiycode', verifyCode);
router.post('/user/update', updateUserInfo);
module.exports = router;

function verifyPhone(req, res, next) {
	var phone_num = req.body.phone;
	if (!phone_num) {
		res.send({status: false, message: 'Phone Number is required!'});
	}
	var code = _createCode();
	var response = {
		phone: phone_num,
		code: code,
		is_new: true
	}
	userService.chkUserRegistered(phone_num).then(result => {
		if (!result) {
			return userService.create({
				phone: phone_num,
				code: code
			});
		} else {
			response.is_new = false;
			return userService.update(phone_num, {
				code: code
			});
		}
	}).then(result => {
	// 	return twilioService.sendSMS(phone_num, code)
	// }).then(result => {
		res.send({status: true, data: response});
	}).catch(err => {
		res.send({status: false, message: err.message});
	})

}

function verifyCode(req, res, next) {
	var phone_num = req.body.phone;
	if (!phone_num) {
		res.send({status: false, message: 'Phone Number is required!'});
	}
	var code = req.body.code;

	userService.getUserbyPhone(phone_num).then(result => {
		if (result.code == code) {
			var uid = result._id;
			userService.update(phone_num, {
				is_active: true,
				last_seen: new Date(),
				code: ""
			});
			res.send({status: true, data: {phone: phone_num, token: "zfewifkl654342lkj=fdsfsflkk3-fdsfeee3"}});
		} else {
			res.send({status:false, message: "Invalid code!"});
		}
	}).catch(err => {
		res.send({status: false, message: err.message});
	})

}

function updateUserInfo(req, res, next) {
	var phone_num = req.body.phone;
	var update_data = req.body.update_data;
	userService.update(phone_num, update_data).then(result => {
		res.send({status: true, data: result});
	}).catch(err => {
		res.send({status: false, message: err.message});
	})
}

function _createCode() {
	var code = "";
	var possible = "0123456789";

	for (var i = 0; i < 6; i++)
		code += possible.charAt(Math.floor(Math.random() * possible.length));

	return code;
}