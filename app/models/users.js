var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	name: String,
	phone: {type: String, required: true, unique: true},
	photo: String,
	status: String,
	created_at: Date,
	updated_at: Date,
	token: String,
	code: String,
	last_seen: Date,
	is_admin: {type: Boolean, default: false},
	is_active: {type: Boolean, default: false}
});

userSchema.pre('save', function(next) {
	var currentDate = new Date();

	this.updated_at = currentDate;

	if (!this.created_at) {
		this.created_at = currentDate;
	}
	next();
});

var User = mongoose.model('User', userSchema);

module.exports = User;