var Q = require('q');
var ObjectId = require('mongodb').ObjectID;

var User = require('app/models/users');

var service = {};

service.create = create;
service.update = update;
service.updatebyId = updatebyId;
service.getUserbyPhone = getUserbyPhone;
service.chkUserRegistered = chkUserRegisteredbyPhone;

module.exports = service;

function create(data) {
	var deferred = Q.defer();

	var newUser = User(data);

	newUser.save(function(err, user) {
		if (err) {
			deferred.reject(err);
		} else {
			deferred.resolve(user);
		}
	});

	return deferred.promise;
}

function update(phone_num, data) {
	var deferred = Q.defer();

	User.findOneAndUpdate({phone: phone_num}, data, function(err, user) {
		if (err) {
			deferred.reject(err);
		} else {
			deferred.resolve(user);
		}
	});

	return deferred.promise;
}

function updatebyId(id, data) {
	var deferred = Q.defer();

	User.findByIdAndUpdate(new ObjectId(id), data, function(err, user) {
		if (err) {
			deferred.reject(err);
		} else {
			deferred.resolve(user);
		}
	});

	return deferred.promise;	
}

function getUserbyPhone(phone_num) {
	var deferred = Q.defer();

	User.findOne({phone: phone_num}, function(err, user) {
		if (err) {
			deferred.reject(err);
		} else if (user) {
			deferred.resolve(user);
		} else {
			deferred.reject({message: "User not found!"});
		}
	});

	return deferred.promise;
}

function chkUserRegisteredbyPhone(phone_num) {
	var deferred = Q.defer();

	User.findOne({phone: phone_num}, function(err, user) {
		if (err) {
			deferred.reject(err);
		} else if (user) {
			deferred.resolve(true);
		} else {
			deferred.resolve(false);
		}
	});

	return deferred.promise;
}

