var Q = require('q');
var config = require('config');

var client = require('twilio')(config.twilio.accountSid, config.twilio.authToken);


var service = {};

service.sendSMS = sendSMS;


module.exports = service;

function sendSMS(receiver, message) {
	var deferred = Q.defer();

	client.messages.create({
		body: message,
		from: config.twilio.sender,
		to: receiver
	}).then(message => {
		deferred.resolve(message.sid);
	}).catch(err => {
		deferred.reject(err);
	});

	return deferred.promise;
}