module.exports = {
	"mongoDB": "MongoDB connection credentials",
	"twilio": {
		"accountSid": "Twilio Account SID",
		"authToken": "Twilio Auth Token",
		"sender": "Twilio verified phone number"
	}
}